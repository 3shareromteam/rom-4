#!/bin/bash
echo "Installing Package"

AEM_USER="admin"
AEM_PASSWORD="admin"
EXIT_STATUS=0
AEM_DEPLOY_HOST="127.0.0.1"
AEM_DEPLOY_PORT="4503"
AEM_DEPLOY_URL="https://${AEM_DEPLOY_HOST}:${AEM_DEPLOY_PORT}"
DEPLOY_FOLDER="/tmp/deploy"
FILE_PATH=$(ls -altr -1 ${DEPLOY_FOLDER}/ROM*.zip | head -1)
FILE_NAME=$(basename "${FILE_PATH}")


deploy_code(){
    echo ""
    echo "*******************************************************************************************"
    echo "* "    
    echo "* DEPLOY CODE: Deploying code ${FILE_NAME} to ${AEM_DEPLOY_URL} package manager"
    echo "*"
    echo "*******************************************************************************************"
    echo "" 

    STATUS=$(curl -u "${AEM_USER}:${AEM_PASSWORD}" -F file=@"${FILE_PATH}" -F name="${FILE_NAME}" -F force=true -F install=true -o /dev/null -w '%{http_code}' ${AEM_DEPLOY_URL}/crx/packmgr/service.jsp 2>/dev/null)

    if [ ${STATUS} != 200 ]
    then
        echo "*******************************************************************************************"
        echo "*"
        echo "* ERROR : AEM code deployment returned a ${STATUS} status code."
        echo "*"
        echo "*******************************************************************************************"
        EXIT_STATUS=1  
        exit 5
    else
      echo "*******************************************************************************************"
      echo "*"
      echo "* INFO: ${STATUS} status code indicates successful deploy"
      echo "*"
      echo "*******************************************************************************************"        
    fi

}

deploy_code

if [[ ${EXIT_STATUS} -eq 0 ]]; then
  exit 0
else
  exit 1
fi