/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/

package com.threeshare.rom.vanity;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.threeshare.rom.search.SearchUtil;
import com.threeshare.rom.util.ROMConstants;

public final class VanityFactory
{
	private static final Logger log = LoggerFactory.getLogger(VanityFactory.class);
	
	public static VanityNode getVanity(ResourceResolver resolver, Resource resource) 
	{
		Node node = resource.adaptTo(Node.class);
		return getVanity(resolver, node);
	}
	
	public static VanityNode getVanity(ResourceResolver resolver, String path) 
	{
		Node node = resolver.resolve(path).adaptTo(Node.class);
		return getVanity(resolver, node);
	}
	
	public static VanityNode getVanity(ResourceResolver resolver, Node vanityNode)
	{
		VanityNode vanity = null;

		try
		{
			if (vanityNode != null)
			{
				Node vanityContentNode = vanityNode.getNode(JcrConstants.JCR_CONTENT);
				
				if (vanityContentNode != null)
				{
					vanity = new VanityNode(resolver, vanityNode);
				}
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}
		
		// Make sure we can access the Channel (see comment in getChannel).
		if (vanity != null && vanity.getConfig() == null)
		{
			vanity = null;
		}

		return vanity;
	}

	public static VanityConfigNode getConfigNode(ResourceResolver resolver, Resource resource) throws Exception
	{
		Node node = resource.adaptTo(Node.class);
		return getConfigNode(resolver, node);
	}
	
	public static VanityConfigNode getConfigNode(ResourceResolver resolver, String path) throws Exception
	{
		Node node = resolver.resolve(path).adaptTo(Node.class);
		return getConfigNode(resolver, node);
	}
	
	public static VanityConfigNode getConfigNode(ResourceResolver resolver, Node configurationNode) throws Exception
	{
		VanityConfigNode configNode = null;

		if (configurationNode != null)
			{
				try
				{
					configNode = new VanityConfigNode(resolver, configurationNode);
				}
				catch (IllegalArgumentException ex)
				{
					log.error(ex.toString(), ex);
					configNode = null;
				}
			}
		
		return configNode;
	}
	
	public static VanityConfigNode[] getConfigNodes (ResourceResolver resolver, String vanityConfigurationPath)
	{
		List<VanityConfigNode> configNodeList = new ArrayList<VanityConfigNode>();
		
		try
		{
			if (resolver != null && vanityConfigurationPath != null && vanityConfigurationPath.length() > 0)
			{
				List<Node> nodes = SearchUtil.propertySearchNode(ROMConstants.NODE_TYPE_PROPERTY, 
						ROMConstants.VANITY_CONFIG_NODE_TYPE, vanityConfigurationPath, resolver);
				
				for (Node node : nodes)
				{
					VanityConfigNode configNode = getConfigNode(resolver, node);
					if (configNode != null)
					{
						configNodeList.add(configNode);
					}
				}
			}
		}
		catch (Exception ex)
		{
			log.error("GetConfigNodes returned an error during configNode retrieval on vanity Config path: " + vanityConfigurationPath, ex);
		}
				
		VanityConfigNode[] configNodeArray = configNodeList.toArray(new VanityConfigNode [configNodeList.size()]); 
		
		return configNodeArray;
	}
}
