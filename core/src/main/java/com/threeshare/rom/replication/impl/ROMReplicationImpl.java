package com.threeshare.rom.replication.impl;

import com.threeshare.rom.replication.ROMReplication;
import org.apache.felix.scr.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.replication.AgentManager;
import com.day.cq.replication.Agent;
import com.threeshare.rom.replication.data.ReplicationAgentData;
import java.util.Map;
import java.util.Set;

@Component
@Service
@Properties({
    @Property(name = "service.description", value = "ROM Backup Data Collector"),
    @Property(name = "service.vendor", value = "3|SHARE")
})
public class ROMReplicationImpl implements ROMReplication {

    @Reference
    private AgentManager agentManager;
    @SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ROMReplicationImpl.class);

    public String checkReplicationQueues() throws Exception {
    	return checkReplicationQueues(null,null);
    }
    
    public String checkReplicationQueues(String exclude, String include) throws Exception {
        ReplicationAgentData agentData = new ReplicationAgentData();
        Map<String, Agent> agentsMap = agentManager.getAgents();
        Set<String> keys = agentsMap.keySet();
        for (String key : keys) {
            Agent anAgent = agentsMap.get(key);
            if (anAgent.isEnabled()) {
            	String agentName = anAgent.getConfiguration().getName();
            	if (exclude != null && include !=null){
            		if (exclude.length() > 0 && include.length() > 0)
            		{
            			if (!agentName.contains(exclude) && agentName.contains(include))
            			{
            				agentData.addAgent(anAgent);
            			}
            		}
            	}
            	else if (exclude != null && exclude.length() > 0){
            		if (!agentName.contains(exclude))
        			{
        				agentData.addAgent(anAgent);
        			}
            	}
            	else if (include != null && include.length() > 0){
            		if (agentName.contains(include))
        			{
        				agentData.addAgent(anAgent);
        			}
            	}
            	else
            	{
            		agentData.addAgent(anAgent);
            	}
            }
        }
        return agentData.isQueueBlockedXML();
    }
    
    public String getReplicationQueueSizes() throws Exception {
    	return getReplicationQueueSizes(null,null);
    }
    
    public String getReplicationQueueSizes(String exclude, String include) throws Exception {
        ReplicationAgentData agentData = new ReplicationAgentData();
        Map<String, Agent> agentsMap = agentManager.getAgents();
        Set<String> keys = agentsMap.keySet();
        for (String key : keys) {
            Agent anAgent = agentsMap.get(key);
            if (anAgent.isEnabled()) {
            	String agentName = anAgent.getConfiguration().getName();
            	if (exclude != null && include !=null){
            		if (exclude.length() > 0 && include.length() > 0)
            		{
            			if (!agentName.contains(exclude) && agentName.contains(include))
            			{
            				agentData.addAgent(anAgent);
            			}
            		}
            	}
            	else if (exclude != null && exclude.length() > 0){
            		if (!agentName.contains(exclude))
        			{
        				agentData.addAgent(anAgent);
        			}
            	}
            	else if (include != null && include.length() > 0){
            		if (agentName.contains(include))
        			{
        				agentData.addAgent(anAgent);
        			}
            	}
            	else
            	{
            		agentData.addAgent(anAgent);
            	}
            }
        }
        return agentData.replicationQueueSizes();
    }
    
}
