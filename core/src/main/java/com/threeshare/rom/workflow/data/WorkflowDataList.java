/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.threeshare.rom.workflow.data;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author rbrown
 */
public class WorkflowDataList {
   private List<WorkflowData> list = new ArrayList<WorkflowData>();

    public List<WorkflowData> getList() {
        return list;
    }
   
    public void addWorkflowData(WorkflowData data) {
        list.add(data);
    }
    
    public String toXML() {
        StringBuilder xml = new StringBuilder();
        xml.append("<prtg>");
        ListIterator<WorkflowData> it = list.listIterator();        
        while (it.hasNext()) {
            xml.append(it.next().toResultXML());
        }
        xml.append("/prtg>");
        return xml.toString();
    }
}
