/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/

package com.threeshare.rom.recaptcha;

public class RecaptchaUtil
{
	private static String publicKey = null;
	
	public static String getRecaptchaPublicKey()
	{
		return publicKey;
	}
	
	public static void setRecaptchaPublicKey(String publicKeySet)
    {
        publicKey = publicKeySet;
    }
}