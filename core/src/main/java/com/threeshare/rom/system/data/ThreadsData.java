package com.threeshare.rom.system.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ThreadsData {
	private long totalThreadCount;
	
	
	public long getTotalThreadCount() {
		return this.totalThreadCount;
	}
	
	public void setTotalThreadCount(long totalThreadCount) {
		this.totalThreadCount = totalThreadCount;
	}
		
	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<prtg>");
		
		sb.append("<result>");
		sb.append("<channel>Total Thread Count</channel>");
		sb.append("<unit>Count</unit>");
		sb.append("<value>" + this.getTotalThreadCount() + "</value>");
		sb.append("</result>");

		sb.append("</prtg>");
		return sb.toString();
	}

}
