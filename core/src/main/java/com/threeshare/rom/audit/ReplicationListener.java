/*
 *  Copyright 2014 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.threeshare.rom.audit;

import java.util.Dictionary;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationAction;

@Component(metatype = true, immediate = true, label = "3|SHARE - Audit - Replication Listener", description = "Replication Listener: Logs replication events.")
@Service(value = EventHandler.class)

public class ReplicationListener implements EventHandler {

	@Property (label = "Auditing Enabled", description = "Option to enable/disable auditing. ", boolValue = true)
	protected static final String AUDIT_ENABLED = "audit.config.enabled";
	
	@Property (label = "Event Topic", description = "Audited Event Topic", value = ReplicationAction.EVENT_TOPIC, propertyPrivate = true )
	protected static final String EVENT_TOPIC = EventConstants.EVENT_TOPIC;
	
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static String eventPathProperty = "paths";
    private static boolean auditingEnabled = true;
    
    public void  handleEvent(final Event event) {
    	if (auditingEnabled) {
	    	String repPaths = java.util.Arrays.toString((String[]) event.getProperty(eventPathProperty));
	    	String repType = event.getProperty(ReplicationAction.PROPERTY_TYPE).toString();
	    	String repUser = event.getProperty(ReplicationAction.PROPERTY_USER_ID).toString();
	    	
	    	log.debug("Replication Event: " + repUser + " " + repType + " " + repPaths);
		}
    }
    
    @SuppressWarnings("unchecked")
    @Activate
    protected void activate(ComponentContext context) {
    	// On activation, retrieve values from OSGI configuration.
    	final Dictionary<String, Object> properties = context.getProperties();
    	auditingEnabled = PropertiesUtil.toBoolean(properties.get(AUDIT_ENABLED),true);
    }
}




