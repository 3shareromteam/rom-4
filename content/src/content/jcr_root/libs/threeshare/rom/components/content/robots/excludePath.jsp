<%@ page import="org.apache.commons.lang.StringEscapeUtils,
                     com.threeshare.rom.search.SearchUtil,
                     java.util.List,
                     java.util.ArrayList"%><%
%><%@include file="/libs/foundation/global.jsp"%><%

    response.setContentType("text/plain");


    //TODO: Add process to automate removePaths and SearchPaths
    List<String> removePaths = new ArrayList<String>();
    removePaths.add("/content/twc");

    List<String> excludePaths = new ArrayList<String>();
    excludePaths.add("/content/twc/en/residential-home/specials");
    excludePaths.add("/content/dam/residential/specials-ola");
    excludePaths.add("/content/campaigns");
    excludePaths.add("/etc/clientcontext/default");
    excludePaths.add("/etc/segmentation/twc-segments/business");
    excludePaths.add("/content/twc/en/business-home/responsive-business");
    excludePaths.add("/responsive-business");
    excludePaths.add("/content/twc/en/business-home/forms");
    excludePaths.add("/content/twc/en/business-home/localization");
    excludePaths.add("/content/twc/en/business-home/reuseable-content");


    for (String excludePath : excludePaths)
    {
    	if (excludePath.endsWith("/"))
    	{
    		excludePath = excludePath.substring(0, excludePath.length() - 1 );
    	}

    	if (excludePath.length() > 1)
    	{
	        %><%="Disallow: " + excludePath + "/"%>
<%
	        for (String removePath: removePaths)
	        {
	            if (excludePath.startsWith(removePath))
	            {
	                String cleanPagePath = excludePath.replace(removePath, "");
	                if (cleanPagePath.length() > 1)
	                {
	                    %><%="Disallow: " + cleanPagePath + "/"%>
<%
	                }
	            }
	        }
    	}
    }
%>
<%

    // Allowed Paths
    List<String> allowPaths = new ArrayList<String>();
    allowPaths.add("/content/dam/business");
	allowPaths.add("/content/dam/*.css$");
	allowPaths.add("/etc/clientlibs/*.css$");
	allowPaths.add("/etc/designs/*.css$");
	allowPaths.add("/etc/clientlibs/*.js$");

    %># Allowed paths
<%
    for (String allowPath : allowPaths)
    {
        %>Allow: <%=allowPath%>
<%

    }
%>
