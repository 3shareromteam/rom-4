<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Component Timing</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>


<style>
	.main {margin:0 auto; width: 940px; text-align: center;}
	.embed .placeHolder {width: 640px; height: 360px; background-color: #000000; margin: 0 auto;}
	.timingDataTextArea {width: 640px; height: 75px; margin: 0 0 25px 0;}
	.iconLink {margin: 0 0 10px 0;}
	.iconLink img {width: 55px;}
	p {margin: 0px;}
</style>

</head>

<body>
	<div class="main">
		<h1>Component Timing Data</h1>
		<p>Paste timing data here</p>
		<textarea class="timingDataTextArea"></textarea>


		<p></p>
		<a href="#" class="iconLink"><button class="loadData" type="button">Refresh Chart</button></a>


		<div id="chart_div" class="chart">
			<div class="placeHolder">
			</div>
		</div>


		<script type="text/javascript">
          google.load("visualization", "1", {packages:["corechart"]});

          drawChart = function(data) 
          {

            var data = google.visualization.arrayToDataTable(data);
    
            var options = 
            {
              chartArea: {top: 0},  
              width: 900,
              height: 3000,
              legend: { position: 'top', maxLines: 3 },
                bar: { groupWidth: '75%' },
              isStacked: true,
            };
            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
            chart.draw(data, options);
          }

		  	$(".iconLink").bind("click", function (){
                var jsonData = $(".timingDataTextArea").val();

                //console.log(jsonData);         
                dataset = JSON.parse(jsonData);

				drawChart(dataset);
		  		return false;

			})


        </script>



	</div>	
</body>
</html>