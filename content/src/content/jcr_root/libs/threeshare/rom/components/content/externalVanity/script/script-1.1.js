var rom = rom || {};

rom.formSubmit = function(){
    $(".form-submit").click(function()
    {
        var formID = $(this).attr("data_submit_target");
        var validation = true;
        var vanityJson = "";
        var errorClass = rom.errorClass;
        var defaultRedirectType = rom.defaultRedirectType;
        var count = 0;

        // Remove Empty vanities
        $(".hasFocus").removeClass("hasFocus");
        rom.removeEmptyVanity();
        
        // Clear any existing error messages before validation
        rom.clearError();
        
        // Open Json String
        vanityJson = vanityJson + "{ \"vanityPost\": [";
        
        // Form Validation
        $(".vanityURL").each(function(){
            var vanityPath = $(this).children(".vanityPath").val().trim();
            var redirectTarget = $(this).children(".redirectTarget").val().trim();
            var nodePath = $(this).children(".vanityPath").attr(rom.NODE_ATTR);
            var vanityCheck = rom.inputDefaultCheck($(this).children(".vanityPath"));
            var targetCheck = rom.inputDefaultCheck($(this).children(".redirectTarget"));
            var redirectType = $(this).children(".checkbox:checked").attr("data_value"); 

            // Check for blank vanity
            if (vanityPath == null || vanityPath.length < 1)
            {
                $(this).addClass(errorClass);
                validation = false;
            }

            // Check for blank Target
            if (redirectTarget == null || redirectTarget.length < 1)
            {
                $(this).addClass(errorClass);
                validation = false;
            }
            
            // Check for blank redirectType
            if (redirectType == null || redirectType.length < 1)
            {
                redirectType = defaultRedirectType;
            }

            // Ignore empty or partially complete fields
            if (!$(this).hasClass("empty"))
            {    

                // Check for default values
                if (vanityCheck || targetCheck)
                {
                    $(this).addClass(errorClass);
                    validation = false;
                }
                
                // Check for valid http address in target
                var urlMatch = regexUrl.test(redirectTarget);
                if (!urlMatch)
                {
                    console.log(redirectTarget + " failed url check validation");
                    $(this).addClass(errorClass);
                    validation = false;
                }

                // Check for spaces in vanity
                if (!regexAlphaNumWithSpecialChar.test(vanityPath))
                {
                    console.log("Failed vanity path space check validation");
                    $(this).addClass(errorClass);
                    validation = false;
                }
                
                // Check for spaces in target
                if (!regexAlphaNumWithSpecialChar.test(redirectTarget))
                {
                    console.log("Failed target path space check validation");
                    $(this).addClass(errorClass);
                    validation = false;
                }
                
                // Only submit dirty fields
                if ($(this).hasClass("isDirty"))
                {
                
                    // Build Json String
                    if(count > 0)
                    {
                        vanityJson = vanityJson + ",";
                    }
                    
                    vanityJson = vanityJson + "{";
                    vanityJson = vanityJson + "\"" + rom.VANITY_URI_JSON_KEY + "\"" + ":\"" + vanityPath + "\",";
                    vanityJson = vanityJson + "\"" + rom.TARGET_JSON_KEY + "\"" + ":\"" + redirectTarget + "\",";
                    vanityJson = vanityJson + "\"" + rom.REDIRECT_TYPE_JSON_KEY + "\"" + ":\"" + redirectType + "\",";
                    vanityJson = vanityJson + "\"" + rom.NODE_PATH_JSON_KEY + "\"" + ":\"" + nodePath + "\"" ;
                    vanityJson = vanityJson + "}";
                    
                    count++;
                    
                };
            };
            
        });
        
        // Close Json String
        vanityJson = vanityJson + "]";
        vanityJson = vanityJson + "}";
        
        if (validation)
        {
            // Validation passed, run form-processing
            $.ajax({
                type: 'POST',
                url: "/bin/threeshare/vanity",
                data: { 
                      vanity: vanityJson,
                      configNode: rom.CONFIG_PATH,
                      requestType: rom.UPDATE_VANITY_REQUEST_TYPE 
                      },
                success : function(data){
                    rom.success("Vanity save complete.");
                    $(".vanityURL.isDirty").removeClass("isDirty");
                      },
                error : function(data){
                    var response = data.responseText;
                    if (response.length < 1)
                    {
                        response = "Error processing update";
                    }
                    rom.error(response);
                },
                complete : function(data){
                },
                cache : false             
            });    
                  
        }
        else
        {
            // Validation failed, Display error message
            rom.error("Please verify highlighted fields");
            return false;
        }
        
        // stop <a> html click handler from running
        return false;
    });
}

rom.error = function(errorMessage){
    $(".statusMessage .title").text(errorMessage).addClass(rom.errorClass);
    $(".statusMessage .title").slideDown();
}

rom.clearError = function(){
    $(".vanityURL").removeClass(rom.errorClass);
    $(".statusMessage .title").slideUp();
    $(".statusMessage .title").text("").removeClass(rom.errorClass);
}

rom.success = function(successMessage){
    rom.clearError()
    $(".statusMessage .title").text(successMessage).removeClass(rom.errorClass);
    $(".statusMessage .title").slideDown();
}

rom.redirect = function(redirectTarget){
    window.location.replace(redirectTarget);
}

rom.onFocus = function () {
    $(".vanityURL input").focus(function(){
        var vanityURLElement = $(this).parent(".vanityURL")[0];
        var defaultValue = $(this).attr("data_default_value");
        var currentValue = $(this).val();

        $(".hasFocus").removeClass("hasFocus");
        $(vanityURLElement).addClass("hasFocus").addClass("isDirty");
        rom.removeEmptyVanity();            
        if (defaultValue == currentValue)
        {
            $(this).val("").removeClass("default");
            if (rom.vanityInputCheck(vanityURLElement))
            {
                if ($(vanityURLElement).hasClass("empty"))
                {
                      $(vanityURLElement).removeClass("empty");
                      rom.addInput(vanityURLElement);
                }
            }
        }
    });    
}

rom.onBlur = function () {
    $(".vanityURL input").blur(function(){
        var defaultValue = $(this).attr("data_default_value");
        var currentValue = $(this).val();
        if (currentValue.length < 1 || rom.inputDefaultCheck(this))
        {
            $(this).val(defaultValue).addClass("default");
        }
        
    });
}

rom.checkbox = function () {
    $("input.checkbox").change(function(){
        
        var vanityURLElement = $(this).parent(".vanityURL")[0];
        $(vanityURLElement).addClass("isDirty");
        
        if ($(this).is(":checked"))
        {
            $(this).siblings("input.checkbox").removeAttr("checked");
        }
        else
        {   
            $(this).siblings("input.checkbox").prop("checked", true);
        }
        
    });
}

rom.remove = function() {
    $(".vanityURL .removeVanity").click(function(){
        var vanityURLElement = $(this).parent(".vanityURL")[0];
        var vanityPath = $(vanityURLElement).children(".vanityPath").attr(rom.NODE_ATTR)
        if (vanityPath == rom.NEW_VANITY)
        {
            $(vanityURLElement).remove();
        }
        else
        {
            json = "{ \"vanityPost\": [";
            json = json + "{";
            json = json + "\"" + rom.NODE_PATH_JSON_KEY + "\"" + ":\"" + vanityPath + "\"" ;
            json = json + "}";
            json = json + "]";
            json = json + "}";
            
            $.ajax({
                type: 'POST',
                url: "/bin/threeshare/vanity",
                data: { 
                      vanity: json,
                      configNode: rom.CONFIG_PATH,
                      requestType: rom.DELETE_VANITY_REQUEST_TYPE
                      },
                success : function(data){
                          $(vanityURLElement).remove();
                      },
                error : function(data){
                          rom.error("Error occurred during vanity delete.")
                      },
                complete : function(data){},
                cache : false             
            });
        }
        return false;
    }); 
}

rom.addInput = function (element) {
    var vanityHTML = $("div.template").html();
    $(element).after(vanityHTML);
    rom.refresh();
}

rom.removeEmptyVanity = function() {
    $(".vanityURL").each(function() {
        if (!$(this).hasClass("empty") && !$(this).hasClass("hasFocus") && rom.vanityInputDefaultCheck(this)) {
            $(this).remove();
        } 
    });
};


rom.inputDefaultCheck = function (inputElement) {
    var inputValue = $(inputElement).val().toLowerCase().trim();
    var inputType = $(inputElement).attr("type");
    
    // Only run default input check on text inputs
    if (inputType == "text")
    {
        var inputDefaultValue = $(inputElement).attr("data_default_value").toLowerCase().trim();
        
        if (inputValue == inputDefaultValue)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return true;
    }
};

rom.vanityInputCheck = function (element) {
    var vanityCheck = rom.inputDefaultCheck($(element).children(".vanityPath"));
    var targetCheck = rom.inputDefaultCheck($(element).children(".redirectTarget"));
    
    if (!vanityCheck || !targetCheck)
    {
        return true;
    }
    else
    {
        return false;
    }
}

rom.vanityInputDefaultCheck = function (element) {
    var vanityCheck = rom.inputDefaultCheck($(element).children(".vanityPath"));
    var targetCheck = rom.inputDefaultCheck($(element).children(".redirectTarget"));
    
    if (vanityCheck && targetCheck)
    {
        return true;
    }
    else
    {
        return false;
    }
}

rom.sort = function() {
    $("a.sort").click(function(){
        var target = window.location.href.split('?')[0];
        var sortElement = $(this).children(".sortIcon");
        if (!$(sortElement).hasClass(rom.sortDescendClass))
        {
            target = target + "?" + rom.sortParam + "=" + rom.sortDescendClass;
        }
        
        window.location.href = target;
        return false;
    })
}

rom.init = function () {
    rom.refresh();
    rom.formSubmit();
    rom.sort();
    rom.checkbox();
}

rom.refresh = function () {
    rom.onBlur();
    rom.onFocus();
    rom.remove();
    rom.checkbox();
}

var regexUrl = /^(https?:\/\/|\/)/;
var regexAlphaNumWithSpecialChar = /^[\S]*$/;

rom.init();