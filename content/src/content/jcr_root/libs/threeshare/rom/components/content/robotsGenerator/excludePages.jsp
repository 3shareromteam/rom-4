<%@include file="/libs/foundation/global.jsp"%><%
response.setContentType("text/plain");
String[] excludePages = properties.get("excludePages",String[].class);

if (excludePages != null && excludePages.length > 0)
{
    %>

# Excluded Pages
<%
    for (String excludePage : excludePages)
    {
        if (excludePage != null && excludePage.length() > 0)
        {
            if (excludePage.endsWith(".html"))
            {
                %>Disallow: <%=resourceResolver.map(excludePage)%> #
<%
            }
            else
            {
                %>Disallow: <%=resourceResolver.map(excludePage)%>.html #
<%
            }
        }
    }
}
%>
