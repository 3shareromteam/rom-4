<%@include file="/libs/foundation/global.jsp" %>
<%

String[] allowPaths = properties.get("allowPaths",String[].class);
String[] excludePages = properties.get("excludePages",String[].class);
String[] excludePaths = properties.get("excludePaths",String[].class);
String[] excludeTags = properties.get("excludeTags",String[].class);
String crawlDelay = properties.get("crawlDelay","10");
String sitemapPath = properties.get("sitemapPath","");
String userAgent = properties.get("userAgent","*");

%>
<h3>General Config</h3>

<table>
	<tbody>

	<tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>User Agent</p></td>
		<td class="specRight layoutBorder">
            <%=userAgent%>
        </td>
    </tr>


	<tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Sitemap Path</p></td>
		<td class="specRight layoutBorder">
            <%=sitemapPath%>
        </td>
    </tr>


	<tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Crawl Delay</p></td>
		<td class="specRight layoutBorder">
            <%=crawlDelay%>
        </td>
    </tr>

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Allowed Paths</p></td>
		<td class="specRight layoutBorder">
        <%
            if (allowPaths != null)
            {
                for(String allowPath : allowPaths)
                {
                    %><p><%=allowPath%></p><%
                }
            }
		%>
        </td>
    </tr>

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Excluded Pages</p></td>
		<td class="specRight layoutBorder">
        <%
            if (excludePages != null)
            {
                for(String excludePage : excludePages)
                {
                    %><p><%=excludePage%></p><%
                }
            }
		%>
        </td>
    </tr>

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Excluded Paths</p></td>
		<td class="specRight layoutBorder">
        <%
            if (excludePaths != null)
            {
                for(String excludePath : excludePaths)
                {
                    %><p><%=excludePath%></p><%
                }
            }
		%>
        </td>
    </tr>

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Excluded Tags</p></td>
		<td class="specRight layoutBorder">
        <%
            if (excludeTags != null)
            {
                for(String excludeTag : excludeTags)
                {
                    %><p><%=excludeTag%></p><%
                }
            }
		%>
        </td>
    </tr>

	</tbody>
</table>

<br>
<div class="Preview">
    <a href="<%= currentPage.getPath() %>/_jcr_content/robots.txt" target="blank">Preview Config</a>
</div>
<br><br><br><br>
