<%@include file="/libs/foundation/global.jsp"%><%@page
import="com.threeshare.rom.search.SearchUtil"%><%@page
import="java.util.List"%><%
response.setContentType("text/plain");
String[] excludePaths = properties.get("excludePaths",String[].class);

if (excludePaths != null && excludePaths.length > 0)
{
    %>

# Excluded Paths
<%
    for (String excludePath : excludePaths)
    {
        if (excludePath != null && excludePath.length() > 0)
        {
        %>Disallow: <%=excludePath%> #
<%
        }
    }
}
%>
